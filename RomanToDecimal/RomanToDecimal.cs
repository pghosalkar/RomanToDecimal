﻿using NUnit.Framework;

namespace RomanToDecimal
{
    public class Roman
    {
        public static int ToDecimal(string roman)
        {
            var result = 0;
            for (int i = 0; i < roman.Length; i++)
            {
                if (i != (roman.Length - 1) && roman[i] == 'C' && roman[i + 1] == 'M')
                {
                    result += 900;
                    i++;
                } else if (i != (roman.Length - 1) && roman[i] == 'C' && roman[i + 1] == 'D')
                {
                    result += 400;
                    i++;
                } else if (i != (roman.Length - 1) && roman[i] == 'X' && roman[i + 1] == 'C')
                {
                    result += 90;
                    i++;
                } else if (i != (roman.Length - 1) && roman[i] == 'X' && roman[i + 1] == 'L')
                {
                    result += 40;
                    i++;
                } else if (i != (roman.Length - 1) && roman[i] == 'I' && roman[i + 1] == 'X')
                {
                    result += 9;
                    i++;
                } else if (i != (roman.Length - 1) && roman[i] == 'I' && roman[i + 1] == 'V')
                {
                    result += 4;
                    i++;
                } else if (roman[i] == 'I')
                {
                    result += 1;
                } else if (roman[i] == 'V')
                {
                    result += 5;
                } else if (roman[i] == 'X')
                {
                    result += 10;
                } else if (roman[i] == 'L')
                {
                    result += 50;
                } else if (roman[i] == 'C')
                {
                    result += 100;
                } else if (roman[i] == 'D')
                {
                    result += 500;
                } else if (roman[i] == 'M')
                {
                    result += 1000;
                }
            }
            return result;
        }
    }

    public class RomanToDecimalTests
    {
        [TestCase("I", ExpectedResult = 1)]
        [TestCase("II", ExpectedResult = 2)]
        [TestCase("III", ExpectedResult = 3)]
        [TestCase("IV", ExpectedResult = 4)]
        [TestCase("V", ExpectedResult = 5)]
        [TestCase("VI", ExpectedResult = 6)]
        [TestCase("VII", ExpectedResult = 7)]
        [TestCase("VIII", ExpectedResult = 8)]
        [TestCase("IX", ExpectedResult = 9)]
        [TestCase("X", ExpectedResult = 10)]
        [TestCase("XI", ExpectedResult = 11)]
        [TestCase("XII", ExpectedResult = 12)]
        [TestCase("XIII", ExpectedResult = 13)]
        [TestCase("XIV", ExpectedResult = 14)]
        [TestCase("XV", ExpectedResult = 15)]
        [TestCase("XVI", ExpectedResult = 16)]
        [TestCase("XVII", ExpectedResult = 17)]
        [TestCase("XVIII", ExpectedResult = 18)]
        [TestCase("XIX", ExpectedResult = 19)]
        [TestCase("XX", ExpectedResult = 20)]
        [TestCase("XXI", ExpectedResult = 21)]
        [TestCase("XXII", ExpectedResult = 22)]
        [TestCase("XXIV", ExpectedResult = 24)]
        [TestCase("XXV", ExpectedResult = 25)]
        [TestCase("XXVI", ExpectedResult = 26)]
        [TestCase("XXIX", ExpectedResult = 29)]
        [TestCase("L", ExpectedResult = 50)]
        [TestCase("LI", ExpectedResult = 51)]
        [TestCase("LIV", ExpectedResult = 54)]
        [TestCase("LIX", ExpectedResult = 59)]
        [TestCase("XL", ExpectedResult = 40)]
        [TestCase("XLI", ExpectedResult = 41)]
        [TestCase("XLIV", ExpectedResult = 44)]
        [TestCase("XLIX", ExpectedResult = 49)]
        [TestCase("MMMMMCDXXXII", ExpectedResult = 5432)]
        public int TestToDecimal(string roman)
        {
            return Roman.ToDecimal(roman);
        }
    }
}